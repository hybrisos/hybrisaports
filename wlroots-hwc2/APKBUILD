# Contributor: Henrik Riomar <henrik.riomar@gmail.com>
# Maintainer: Drew DeVault <sir@cmpwn.com>
pkgname=wlroots-hwc2
pkgver=0.11.0
pkgrel=3
pkgdesc="A modular Wayland compositor library"
url="https://github.com/Danct12/wlroots"
arch="armv7 aarch64"
license="MIT"
options="!check" # contains no test suite
provides="wlroots=$pkgver-r$pkgrel"
install_if="wlroots libhybris"

makedepends="
	elogind-dev
	eudev-dev
	libcap-dev
	libinput-dev
	libxcb-dev
	libxkbcommon-dev
	mesa-dev
	meson
	ninja
	pixman-dev
	wayland-dev
	wayland-protocols
	xcb-util-image-dev
	xcb-util-wm-dev
	xkeyboard-config
	android-headers-9.0
	bsd-compat-headers
	libhybris-9.0
	libhybris-dev
"

subpackages="$pkgname-dev"
_commit="881dd1603b785e3eed7aeab4fe990ee2ff59937d"
source="
	$pkgname-$_commit.tar.gz::https://github.com/hybris-mobian/wlroots/archive/$_commit.tar.gz
	fix-include-path.patch
	wlroots-hwc2-export.sh	
	"

builddir="$srcdir/wlroots-$_commit"

build() {
	abuild-meson \
		-Dlogind=enabled \
		-Dlogind-provider=elogind \
		-Dexamples=false \
		. build
	ninja -C build
}

package() {
	DESTDIR="$pkgdir" ninja -C build install
	install -D -m644 "$srcdir"/wlroots-hwc2-export.sh \
		"$pkgdir"/etc/profile.d/wlroots-hwc2-export.sh
}

sha512sums="c9eca077f150d517d868b2c78cf0e2ace385ab5a40bc15a5e12c9edf444031190d00957bd1be68bdee2456b7e9d18532dc6ded2ba5981d9392e9ce2c50f352fd  wlroots-hwc2-881dd1603b785e3eed7aeab4fe990ee2ff59937d.tar.gz
8b0b1a41e69c49aa6e5ee361c0c5f61e3d384ad69cfc23321c31d90ef2b3b936a6a605d47767c5fe772096ed087abc5e1631351a0c713c48f062d9b5a99432de  fix-include-path.patch
8588c8c7ae9c2be5e5027f6e9730c1db96d3504f16268acc564778f291fabb12c1c7e52ee867c31752bd2c3e77415fc6038ce2ad23c1ae7045806957e27930d9  wlroots-hwc2-export.sh"
